FROM node:12-alpine
COPY . /app
WORKDIR /app
RUN apk update
RUN apk add git
RUN yarn global add vsce
RUN yarn global add standard-version
RUN yarn global add webpack
#RUN touch ~/.netrc
#RUN echo "machine gitlab.com login alicqin password qazwsx12" > ~/.netrc
RUN npm install
CMD /bin/sh
