
# AOP Development VSC extension!
## Overview
All your need for AOP conversion and development (create/edit views, validation, visual design, guideline, code generation, ...).

## Completed Features
* New tree view to display filtered content of profile
* Easy names for views and updated automatically
* Combine GV, ED, JSON file together
* Smart icons are adopt for different files.
* Convert PV to DV automatically (partial)

## Todo Features
* Syntax error reporting
* General code completion
* Auto-close tags
* Automatic node indentation
* Symbol highlighting
* Document folding
* Document links
* Document Formatting
* Visual design
* AOP Development guideline
* Toolbox to generate code automatically

# Guideline
Open AOP_WTUDC_Conversion\AOP_VSC_Ext\Training for latest training.
# Build
Open AOP_WTUDC_Conversion\AOP_VSC_Ext\Build for latest build.

## Team Members
Alley Xu Gai 
Horn Qinghong Liu 
Joyce Jun Wang 
Ken Fuyi Jin 
Jeff Chenjin Qin