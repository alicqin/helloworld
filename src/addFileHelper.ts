/// generatedViewExplorer.ts
/// It is a new explorer for GV,ED view.
/// revision:
/// 03/15/2021      Alley Gai    Create new GV,ED files.

import * as vscode from 'vscode';
import { TreeDependencies } from './treeDependenciesProvider';
import * as fs from "fs";
import { ConfigurationFileHelper } from './configFileHelper';

export class AddFileHelper {
    private dataProvider: TreeDependencies | null;

    constructor(dataProvider: TreeDependencies) {
        this.dataProvider = dataProvider;
    }

    public addNew(): void {
        let newId = ConfigurationFileHelper.newGuid();
        while (ConfigurationFileHelper.isFileExist(newId)) {
            newId = ConfigurationFileHelper.newGuid();
        }
        this.addGV(newId);
        this.addED(newId);
        this.addJson(newId);
    }

    private addGV(newId: string) {
        if (this.dataProvider) {
            fs.writeFile(this.dataProvider.getWorkspaceRoot() + '/GV_' + newId.toUpperCase() + '.xml', 'Hello content!', function (err) {
                if (err) throw err;
                console.log('New GV file added!');
            });
        }
    }
    private addED(newId: string) {
        if (this.dataProvider) {
            fs.writeFile(this.dataProvider.getWorkspaceRoot() + '/ED_' + newId.toUpperCase() + '.xml', 'Hello content!', function (err) {
                if (err) throw err;
                console.log('New ED file added!');
            });
        }
    }
    private addJson(newId: string) {
        if (this.dataProvider) {
            fs.writeFile(this.dataProvider.getWorkspaceRoot() + '/GV_' + newId.toUpperCase() + '.ENU.json', 'Hello content!', function (err) {
                if (err) throw err;
                console.log('New ENU Json file added!');
            });
        }
    }
}