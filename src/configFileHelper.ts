/// configFileHelper.ts
/// It is a base helper to represents the shared content in configuration file, for example,
/// description, file type ...
/// revision:
/// 02/08/2021      Jeff Qin    Create new tree view with GV files.
/// 03/04/2021      Jeff Qin    Adapt xPath to parse xml file.
/// 03/18/2021      Alley Gai   Add uuid for generate guid
/// 03/24/2021      Jeff Qin    Add new methods to modify xml file.

import * as fs from "fs";
import {DOMParserImpl as Dom, 
    NodeImpl as Node, 
    XMLSerializerImpl as Serializer,
    ElementImpl as Element,
    DocumentImpl as Document
    } from 'xmldom-ts';
import * as xpath from 'xpath-ts';
import { HelperConstants } from './helperConst';
import { v4 as uuidv4 } from 'uuid';

// configuration file prefixes
export const GV_PREFIX = "gv_";
export const ED_PREFIX = "ed_";
export const GCV_PREFIX = "gcv_";
export const DV_PREFIX = "dv_";

// file extension
const FILE_XML = "xml";
const FILE_JSON = "json";

// constant variables
const GUID_EX = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;

export class ConfigurationFileHelper {
    /// members
    configDom: any = null;

    constructor(private filePath: string) {
    }

    init(): void {
        if (this.configDom === null) {
            console.log("json content is not available.");
            try {
                const xmlContent = fs.readFileSync(this.filePath, "utf8");
                if(xmlContent){
                    this.configDom = new Dom().parseFromString(xmlContent);
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    // get xml node by name.
    getXmlNodes(name: string): Node[] {
        if (this.configDom === null) {
            this.init();
        }
        let nodes: Node[] = [];
        try {
            nodes = xpath.select(name, this.configDom) as Node[];
        }
        catch (error) {
            console.log(error);
        }
        return nodes;
    }

    // get xml node by name.
    getFirstNodeValue(name: string): string {
        let value = "";
        const nodes = this.getXmlNodes(name);
        if (nodes.length > 0) {
            const item = nodes[0];
            value = item.firstChild.nodeValue;
        }
        return value;
    }

    // add child under target node.
    addChildNode(parent: Node, name: string, value: string): Node|null {
        if(parent == null || name == null)
            return null;
        // create and append new element.
        const doc = this.configDom as Document;
        if(doc){
            const newElement = doc.createElement(name);
            if(newElement)
            {
                newElement.textContent = value;
                return parent.appendChild(newElement);
            }
        }
        return null;
    }
    // set attribute value for target node. If attribute is not
    // available, add the attribute.
    setNodeAttribute(node: Node|null, name: string, value: string):void {
        if(node == null || name == null)
            return;
        const element = node as Element;
        if(element){
            element.setAttribute(name, value);
        }
    }
    // set value for target node.
    setNodeValue(node: Node|null, value: string): void {
        if(node == null)
            return;
        const element = node as Element;
        if(element){
            if(value)
                element.textContent = value;
            else
                element.textContent = "";
        }
    }
    // save configuration file.
    saveFile(): void {
        if(this.configDom){
            const serializer = new Serializer();
            const xmlContent = serializer.serializeToString(this.configDom);
            const doc = this.configDom as Document;
            doc.styleSheets
            fs.writeFileSync(this.filePath, xmlContent, "utf8");
        }
    }

    // get value of description section.
    getDescription():string {
        const desc = this.getFirstNodeValue(HelperConstants.Description);
        return desc;
    }

    // get guid from file name or file content.
    getGuid(): string {
        let guidString = "";
        guidString = ConfigurationFileHelper.getGUID(this.filePath);
        if (guidString === "") {
            // read from content.
            guidString = this.getFirstNodeValue(HelperConstants.ViewType);
        }
        return guidString;
    }

    /// Static members
    // is it a configuration file?
    static isConfigurationFile(fileName: string): boolean {
        const prefixes: Array<string> = [GV_PREFIX, ED_PREFIX, GCV_PREFIX];
        for(const prefix of prefixes)
        {
            if(-1 !== fileName.toLowerCase().indexOf(prefix)){
                return true;
            }
        }
        return false;
    }

    // is it a configuration file?
    static isTargetFile(fileName: string, prefix: string): boolean {
        if (fileName) {
            if (-1 !== fileName.toLowerCase().indexOf(prefix) &&
                fileName.toLowerCase().endsWith("json") !== true) {
                return true;
            }
        }
        return false;
    }

    // get guid from file name.
    static getGUID(fileName: string): string {
        let guidString = "";
        const lowerName = fileName.toLowerCase();
        const results = lowerName.match(GUID_EX);
        if (results !== null && results.length > 0) {
            guidString = results[0];
        }
        return guidString;
    }

    static newGuid(): string {
        const guid: string = uuidv4();
        return guid.toUpperCase();
    }

    static isFileExist(newId: string): boolean {
        return fs.existsSync("GV_" + newId + ".xml") &&
            fs.existsSync("ED_" + newId + ".xml") &&
            fs.existsSync("GV_" + newId + ".ENU.json");
    }

    static getExtension(filename: string): string | undefined {
        return filename.split('.').pop();
    }

    static isAcceptFile(fileName: string): boolean {
        const ext = ConfigurationFileHelper.getExtension(fileName);
        return ext ? (HelperConstants.FILE_TREE_EXT_ACCEPTLIST.includes(ext.toLowerCase())) : false;
    }

    getDevicesViews(): string[]  {
        let value = "";
        const nodeNames:string[] = [];
        const nodes = this.getXmlNodes(HelperConstants.ViewSets);
        nodes.forEach(item => {
            const ele = item as Element;
            value = ele.getAttribute('ID') as string;
            nodeNames.push(value)            
        });
        return nodeNames;
    }
}