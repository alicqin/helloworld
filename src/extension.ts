import * as vscode from 'vscode';
import {GeneratedViewExplorer} from './generatedViewExplorer';

export function activate(context: vscode.ExtensionContext): void {
	console.log('Congratulations, your extension "aop-vsce" is now active!');

	// register views
	new GeneratedViewExplorer(context);
}

export function deactivate(): void {
	console.log("Deactivate extension");
}
