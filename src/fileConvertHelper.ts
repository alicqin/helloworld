/// fileConvertHelper.ts
/// It is for converting files
/// revision:
/// 03/25/2021      Joyce Wang     Do file conversion

import * as vscode from 'vscode';
import { TreeDependencies } from './treeDependenciesProvider';
import * as fs from "fs";
import { ConfigurationFileHelper } from './configFileHelper';
import { pathToFileURL, resolve } from 'url';

export class ConvertFiles {
    private dataProvider: TreeDependencies | null;

    constructor(dataProvider: TreeDependencies) {
        this.dataProvider = dataProvider;
    }

    public async PVtoDV(fullfileName:string): Promise<void> {
        var splitted = fullfileName.split("\\");
        var fileName = splitted[splitted.length - 1];

        if (ConfigurationFileHelper.getExtension(fileName)?.toUpperCase() != "XML")
        {
            console.log("It is not a PV file");
            return;
        }

        if (fileName.substr(0, 2) != "PV"){
            console.log("It is not a PV file");
            return;
        }

        var workPath = __dirname + "\\..\\buildspace\\"; 
        var pvpdpath = workPath + "temp";

        var DVName = "DV_" + fileName.substr(2, fileName.length - 2);
        var dest = fullfileName.replace(fileName, DVName);

        if (fs.existsSync(dest)){
            console.log(dest + " is already existed");
            return;
        }

        try{
            fs.mkdirSync(pvpdpath);
        }
        catch{}

        //Copy the PV file to worksapce to do the conversion
        fs.copyFileSync(fullfileName, pvpdpath + "\\" + fileName);
     
        //Run buildTool to convert PV to DV
        const { exec } = require('child_process');
        var cmd = workPath + "buildtool\\RA.WTUDC.BuildTool.exe -source=" + pvpdpath + " -destination=" +pvpdpath;
        exec(cmd);

        await this.wait(1000);

        //Copy DV back to folder
        fs.copyFileSync(pvpdpath + "\\" + DVName, dest);

        //Delete the files in temp folder
        this.delFile(pvpdpath);

        console.log("DV file is created: " + dest);

    }
    private wait(ms:number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    };


    public delFile(path:string) {
        if (fs.existsSync(path)) {
            if (fs.statSync(path).isDirectory()) {
                let files = fs.readdirSync(path);
                files.forEach((file, index) => {
                    let currentPath = path + "/" + file;
                    if (fs.statSync(currentPath).isDirectory()) {
                        this.delFile(currentPath);
                    } else {
                        fs.unlinkSync(currentPath);
                    }
                });
                }
            } else {
                fs.unlinkSync(path);
            }
        }
}