/// generatedViewExplorer.ts
/// It is a new explorer for generated view.
/// revision:
/// 03/10/2021      Jeff Qin    Create new tree view with GV files.

import * as vscode from 'vscode';
import { TreeDependencies, Dependency } from './treeDependenciesProvider';
import { AddFileHelper } from './addFileHelper';
import { ConvertFiles } from './fileConvertHelper';

export class GeneratedViewExplorer {
    private gvViewer: vscode.TreeView<Dependency> | null;
    private dataProvier: TreeDependencies | null;

    constructor(context: vscode.ExtensionContext) {
        this.gvViewer = null;
        this.dataProvier = null;
        this.createView();
        this.createCommands(context);
    }

    createView(): void {
        // get root path at first
        let rootPath = "";
        if (vscode.workspace.workspaceFolders && vscode.workspace.workspaceFolders?.length > 0) {
            rootPath = vscode.workspace.workspaceFolders[0].uri.fsPath;
        }
        this.dataProvier = new TreeDependencies(rootPath);
        this.gvViewer = vscode.window.createTreeView('GeneratedViews', { treeDataProvider: this.dataProvier });
    }

    createCommands(context: vscode.ExtensionContext): void {
        // register refresh command
        const refresh = vscode.commands.registerCommand('aop-vsce.refreshEntry', () => {
            if (this.dataProvier)
                this.dataProvier.refresh();
        });
        context.subscriptions.push(refresh);

        // register add new GV ED command
        const addNew = vscode.commands.registerCommand('aop-vsce.addGV', () => {
            if (this.dataProvier) {
                new AddFileHelper(this.dataProvier).addNew();
            }
        });
        context.subscriptions.push(addNew);


        const openSourceFile = vscode.commands.registerCommand('aop-vsce.openSourceFile',
            filePath => {
                vscode.workspace.openTextDocument(filePath).then(doc => {
                    vscode.window.showTextDocument(doc);
                })
            });
        context.subscriptions.push(openSourceFile);

        // register right click 
        const launchCustomProcess = vscode.commands.registerCommand('aop-vsce.launchCustomProcess', 
            fileName => {
            if (this.dataProvier) {
                new ConvertFiles(this.dataProvier).PVtoDV(fileName._fsPath);
                this.dataProvier.refresh();
            }
        });
        context.subscriptions.push(launchCustomProcess);
    }
}