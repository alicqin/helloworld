import { resolveCliPathFromVSCodeExecutablePath } from "vscode-test";

export class HelloPromise {
    constructor(){}
    doTest():void{
        console.log("测试开始：==================");
        //this.testCallback();
        //this.testPromise();
        //this.testPromiseChain();
        //this.testPromiseError();
        //this.testPromiseTimer();
        this.testPromiseSequence();
        console.log("测试结束：==================");

    }

    testCallback():void{
        this.createAudioFileAsync("settings", this.successCallback, this.failureCallback);
    }
    testPromise():void{
        this.createAudioFileAsyncWithPromise("settings").then(this.successCallback, this.failureCallback);
    }
    // 成功的回调函数
    successCallback(result:string):void {
        console.log("音频文件创建成功: " + result);
    }
  
    // 失败的回调函数
    failureCallback(error:string | null): void {
        console.log("音频文件创建失败: " + error);
    }
    
    createAudioFileAsync(audioSettings:string, successCallback:(res:string)=>void, failureCallback:(err:string)=>void){
        console.log("音频文件设置:" + audioSettings);
        successCallback("success");
        failureCallback("failed");
    }

    createAudioFileAsyncWithPromise(audioSettings:string):Promise<any> {
        console.log("音频文件设置:" + audioSettings);
        return new Promise((resolve, reject):void=>{
            resolve("success");
            reject("failed");
        });
    }

    testPromiseChain():void {
        /* this.doSomeThing(10).then((result)=>{
            return this.doAnotherThing(result);
        }).then((result)=>{
            return this.doThirdThing(result);
        }).then((result)=>{
            console.log("finally:" + result);
        }); */
        // simplify
        this.doSomeThing(10)
        .then(result=>this.doAnotherThing(result))
        .then(result=>this.doThirdThing(result))
        .then(result=>console.log("finally:" + result));
    }

    doSomeThing(num:number):Promise<number>{
        console.log("do some thing:" + num);
        return new Promise((resolve, reject):void=>{
            resolve(++num);
            reject(num);
        });
    }

    doAnotherThing(num:number):number{
        console.log("do another thing:" + num);
        return ++num;
    }

    doThirdThing(num:number):number{
        console.log("do third thing:" + num);
        num++;
        return num;
    }

    testPromiseError():void {
        try {
            this.doSomeThing(10)
            .then(result=>this.doAnotherThing(result))
            .then(result=>this.doThirdThing(result))
            .then(result=>this.doThingException(result))
            .catch(err=>console.log("error:" + err))
            .then(result=>console.log("finally:" + result));
        }
        catch(error){
            console.log("testPromiseError error:" + error);
        }
    }

    doThingException(num:number):void{
        throw new Error("doThingException");
    }

    testPromiseTimer():void {
        const wait = (ms:any) => new Promise(resolve => setTimeout(resolve, ms));
        wait(10000).then(()=>this.saySomething("10 seconds")).catch(this.failureCallback);
    }

    saySomething(thing:string):void {
        console.log("saySomething:" + thing);
    }

    testPromiseSequence():void {
        const wait = (ms:any) => new Promise(resolve => setTimeout(resolve, ms));
        wait(10000).then(() => console.log(4));
        Promise.resolve().then(() => console.log(2)).then(() => console.log(3));
        console.log(1); // 1, 2, 3, 4
    }
}