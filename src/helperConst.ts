/// helperConst.ts
/// The class is used to define the constant variables.
/// revision:
/// 03/04/2021      Jeff Qin    Create new tree view with GV files.

export class HelperConstants {
    static readonly Description:string = "/GeneratedView/Header/Description";
    static readonly ViewType:string = "/GeneratedView/Header/ViewType";
    static readonly ViewStruct:string = "/GeneratedView/ViewStructure";
    static readonly ViewHeader:string = "/GeneratedView/Header";
    static FILE_TREE_EXT_ACCEPTLIST = ["json", "xml"];//only added in lowwer case 
    static readonly ViewSets:string = "/DeviceSpecification/ViewSets/ViewSet";
} 