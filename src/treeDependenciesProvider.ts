/// configFileHelper.ts
/// It is a base helper to represents the shared content in configuration file, for example,
/// description, file type ...
/// revision:
/// 02/08/2021      Jeff Qin    Create new tree view with GV files.

import * as vscode from "vscode";
import * as fs from "fs";
import * as path from "path";
import { ConfigurationFileHelper, GV_PREFIX, ED_PREFIX, DV_PREFIX } from "./configFileHelper";

export class TreeDependencies implements vscode.TreeDataProvider<Dependency> {
    private _onDidChangeTreeData: vscode.EventEmitter<Dependency | undefined | null | void> = new vscode.EventEmitter<Dependency | undefined | null | void>();
    readonly onDidChangeTreeData: vscode.Event<Dependency | undefined | null | void> = this._onDidChangeTreeData.event;
    private fileWatcher: vscode.FileSystemWatcher;
    constructor(private workspaceRoot: string) { 
        this.fileWatcher = vscode.workspace.createFileSystemWatcher("**/*.*");
        this.fileWatcher.onDidChange(() => this.refresh());
        this.fileWatcher.onDidCreate(() => this.refresh());
        this.fileWatcher.onDidDelete(() => this.refresh());
    }

    public getTreeItem(element: Dependency): vscode.TreeItem {
        return element;
    }

    public getChildren(element?: Dependency): Thenable<Dependency[]> {
        if (!this.workspaceRoot) {
            vscode.window.showInformationMessage('No dependency in empty workspace');
            return Promise.resolve([]);
        }

        if (element) {
            return this.getDependentFiles(element);
        } else {
            // read all files in target directory
            return this.getTopLevelFiles(this.workspaceRoot);
        }
    }

    public getWorkspaceRoot(): string {
        return this.workspaceRoot;
    }

    private pathExists(p: string): boolean {
        try {
            fs.accessSync(p);
        } catch (err) {
            return false;
        }
        return true;
    }

    public refresh(): any {
        this._onDidChangeTreeData.fire(null);
    }

    private readDir(path: string): string[] {
        let fileList: string[] = [];
        try {
            fs.readdirSync(path).forEach(file => {
                fileList.push(file);
            });
        } catch (err) {
            return fileList;
        }
        return fileList;
    }
    private async getTopLevelFiles(rootPath: string): Promise<Dependency[]> {
        const depConfig: Dependency[] = await this.getConfigurationFiles(rootPath)
        const depCommon: Dependency[] = await this.getCommonFiles(rootPath, depConfig)
        const dependencies = depConfig.concat(depCommon)
        return dependencies
    }
    
    private async getConfigurationFiles(rootPath: string): Promise<Dependency[]> {
        let dependencies: Dependency[] = [];
        await this.readDir(rootPath).forEach(async fileName => {
            if (ConfigurationFileHelper.isConfigurationFile(fileName)) {
                if (ConfigurationFileHelper.isTargetFile(fileName, GV_PREFIX) === true) {
                    let filePath = path.join(rootPath, fileName);
                    let dep = await this.createDependency(fileName, filePath);
                    dependencies.push(dep);
                    console.log("Add dependencies: " + fileName);
                }
            }      
        }); 
        return dependencies
    }
    private async getCommonExclusionList(configDep: Dependency[], counted: string[])
    {
        await configDep.forEach(async (element) => {
            if (counted.includes( element.fileName )) {return;}//continue;

            counted.push(element.fileName);   
            const subConfigDep = await this.getDependentFiles(element);           
            if (subConfigDep.length > 0) 
            {
               await this.getCommonExclusionList(subConfigDep, counted);
            }    
        });

    }

    private async getCommonFiles(rootPath: string, configDep: Dependency[] ): Promise<Dependency[]> {
        // eslint-disable-next-line prefer-const
        let exceptList: string[] = [];
        await this.getCommonExclusionList(configDep, exceptList);

        // eslint-disable-next-line prefer-const
        let dependencies: Dependency[] = [];
        await this.readDir(rootPath).forEach(async (fileName) => {
            if (ConfigurationFileHelper.isAcceptFile(fileName))
            {
                if (!exceptList.includes(fileName)) {
                    const filePath = path.join(rootPath, fileName);
                    const dep = await this.createDependency(fileName, filePath);
                    dependencies.push(dep);
                    console.log("Add dependencies common file: " + fileName);
                }
            }
        });
        return dependencies
    }

    private getDependentFiles(element: Dependency): Promise<Dependency[]> {
        if(ConfigurationFileHelper.isTargetFile(element.fileName, GV_PREFIX) )
            return this.getGVDependentFiles(element);
        else if(ConfigurationFileHelper.isTargetFile(element.fileName, DV_PREFIX) )
            return this.getDVDependentFiles(element);
        else
            return new Promise(resolve => {resolve([])});
    }

    private getDVDependentFiles(element: Dependency): Promise<Dependency[]> {
        return new Promise(resolve => {
            const dependencies: Dependency[] = [];
            if (element && element.filePath) {
                const helper = new ConfigurationFileHelper(element.filePath);
                const views = helper.getDevicesViews();
                views.forEach(view => {
                    const title = 'ViewSet ID="' + view + '"';
                    this.createDVDependency(title, element.fileName, element.iconName, element.filePath).then(result => {
                        dependencies.push(result);
                        resolve(dependencies);
                    });                    
                });
            }
        });   
    }     

    private getGVDependentFiles(element: Dependency): Promise<Dependency[]> {
        return new Promise(resolve => {
            let dependencies: Dependency[] = [];
            if (element && element.filePath) {
                ///element.collapsibleState = vscode.TreeItemCollapsibleState.Expanded;
                let guid = ConfigurationFileHelper.getGUID(element.filePath);
                this.readDir(this.workspaceRoot).forEach(fileName => {
                    if (element.fileName !== fileName && ConfigurationFileHelper.isConfigurationFile(fileName)) {
                        if (-1 !== fileName.toLowerCase().indexOf(guid)) {
                            let filePath = path.join(this.workspaceRoot, fileName);
                            this.createDependency(fileName, filePath).then(result => {
                                dependencies.push(result);
                                resolve(dependencies);
                            });
                        }
                    }
                });
            }
        });   
    }     

    private createDependency(fileName: string, filePath: string): Thenable<Dependency> {
        return new Promise<Dependency>(resolve => {
            let icon_name = "dependency.svg";
            if (ConfigurationFileHelper.isTargetFile(fileName, GV_PREFIX) || 
            ConfigurationFileHelper.isTargetFile(fileName, DV_PREFIX)) {
                let helper = new ConfigurationFileHelper(filePath);
                let desc = helper.getDescription();
                icon_name = "dependency_GV.svg";
                resolve(new Dependency(desc, fileName, filePath, helper, icon_name, vscode.TreeItemCollapsibleState.Collapsed));
            }
            else {
                if (ConfigurationFileHelper.isTargetFile(fileName, ED_PREFIX))
                {
                    icon_name = "dependency_ED.svg";
                }
                else if (fileName.toLowerCase().endsWith("json") == true)
                {
                    icon_name = "dependency_JSON.svg";
                }
                let helper = new ConfigurationFileHelper(filePath);
                resolve(new Dependency(fileName, fileName, filePath, helper, icon_name, vscode.TreeItemCollapsibleState.None));
            }
        });
    }

    private createDVDependency(name: string, fileName: string, iconName: string, filePath: string): Thenable<Dependency> {
        return new Promise<Dependency>(resolve => {
            const helper = new ConfigurationFileHelper(filePath);
            resolve(new Dependency(name, fileName, filePath, helper, iconName, vscode.TreeItemCollapsibleState.None));
        });
    }    
}

export class Dependency extends vscode.TreeItem {
    constructor(
        public readonly label: string,
        public readonly fileName: string,
        public readonly filePath: string,
        public readonly helper: ConfigurationFileHelper,
        public readonly iconName:string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState
    ) {
        super(label, collapsibleState);
        this.tooltip = `${this.fileName}`;
        this.description = this.fileName;
    }

    iconPath = {
        light: path.join(__filename, '', '', 'resources', 'light', this.iconName),
        dark: path.join(__filename, '..', '..', 'resources', 'dark', this.iconName)
    };

    command = {
        command: 'aop-vsce.openSourceFile',
        title: '',
        arguments: [this.filePath]
    };    
}